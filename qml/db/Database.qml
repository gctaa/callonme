/*
 * Copyright (C) 2022  Jeffrey Elkner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * callonme is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import QtQuick.LocalStorage 2.7 as Sql

Item {
  id: root 

  // list model data and functions
  property var classlistsModel: ListModel{}
  property string currentClass: ""
  property var currentClassList: Array([])

  function listStudents() {
    var outputStr = ""
    if (currentClassList) {
      for (var i=0; i<currentClassList.length; i++ ) {
        outputStr += currentClassList[i] + '\n'
      }
    }
    return outputStr ? outputStr : "No students in this class"
  }

  signal classAdded(var classname)
  signal classRemoved(var classname)

  // connection details
  property var db
  property string dbName: "ClassListsDB"
  property string dbVersion: "1.0"
  property string dbDescription: "Lists of student names by class"
  property int dbEstimatedSize: 100000
  property string classListsTable: "ClassLists"

  Component.onCompleted: {
    init()
  }

  function db_test_callback(db){/* do nothing */}
  function init() {
    // open database
    db = Sql.LocalStorage.openDatabaseSync(
           dbName, dbVersion, dbDescription, dbEstimatedSize
         )
   
    // Create classlists table if needed
    try {
      db.transaction(function(tx) {
        tx.executeSql("CREATE TABLE IF NOT EXISTS "+classListsTable+" ("
                      +"classid INTEGER PRIMARY KEY,"
                      +"classname TEXT NOT NULL, students TEXT NOT NULL)"
                     )
      });
    } catch (e1) {
      console.error("Error creating table '"+classListsTable+"': "+e1)
    }

    // read data 
    try {
      var rows
      db.transaction(function(tx) {
        rows = tx.executeSql(
          "SELECT classid, classname FROM "+classListsTable
        ).rows
      })
      // update ListModel
      for (var i=0; i<rows.length; i++) {
        classlistsModel.append({
            "classid": rows[i].classid,
            "classname": rows[i].classname,
        })
      }
      classlistsModelChanged()
      // set current classname and student list to first one
      if (classlistsModel.count > 0) {
        root.currentClass = classlistsModel.get(0).classname
        var classid = classlistsModel.get(0).classid
        loadClassList(classid)
      }
    } catch (e2) {
      console.error("Error selecting classes: " + e2)
    }
  }

  function addClass(classname) {
    try {
      var classid 
      db.transaction(function(tx) {
        var results = tx.executeSql(
          "INSERT INTO '"+classListsTable
          +"' (classname, students) VALUES(?, ?)",
          [classname, JSON.stringify([])]
        )
        classid = Number(results.insertId) 
        classlistsModel.append(
        {
          "classid": classid,
          "classname": classname
        })
      })
      console.log(classname + " successfully added with classid: " + classid)
      return classid
    } catch (e3) {
      console.error("Error adding class: " + e3)
    }
  }

  function loadClassList(classid) {
    var rows
    db.transaction(function(tx) {
      rows = tx.executeSql(
        "SELECT students from "+classListsTable
        +" WHERE classid = ?",
        classid
      ).rows
    })
    root.currentClassList = JSON.parse(rows.item(0).students)
  }

  function saveClassList(classid, students) {
    try {
      db.transaction(function(tx) {
        var studentsStr = JSON.stringify(students)
        console.log("studentsStr: " + studentsStr + " with classid " + classid)
        tx.executeSql(
          "UPDATE "+classListsTable
          +" SET students = ?"
          +" WHERE classid = ?",
          [studentsStr, classid]
        )
      })
    } catch (e5) {
      console.error("Error adding student list: " + e5);
    }
  }

  function loadSampleData() {
    saveClassList(
      addClass("Interns"),
      ['Freena', 'Thomas', 'Janet', 'Daniel', 'Shallon', 'Mulbah']
    )
    saveClassList(
      addClass("AM"),
      ["Desiree", "Comfort", "Titus", "Aminata", "Kollie", "Exodus",
       "Eddie", "Yawa"]
    )
    saveClassList(
      addClass("Midday"),
      ["Fatumata", "Rafeyatu", "Victoria", "Bendu", "Jallah", "Prince",
       "Gabriel", "Abdulahi", "Bill", "Abraham", "Junior", "Laye"]
    )
    saveClassList(
      addClass("PM"),
      ["Sando", "Philomena", "Maryann", "Victoria", "Gabriel", "Saah",
       "Taddymar"]
    )
  }
}