/*
 * Copyright (C) 2022  Jeffrey Elkner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * callonme is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import QtQuick.LocalStorage 2.7
import Ubuntu.Components 1.3

Page {
  anchors.fill: parent

  header: PageHeader {
    id: header
    title: i18n.tr('Select Class')
  }

  Rectangle {
      anchors {
        top: header.bottom
      }
      width: 180; height: 200
      Component {
          id: classlistDelegate
          Item {
              width: 180; height: 40
              Column {
                  Text { text: classname }
              }
          }
      }
      ListView {
          anchors.fill: parent
          model: db_connector.classlistsModel
          delegate: classlistDelegate
          highlight: Rectangle { color: "lightsteelblue"; radius: 5 }
          focus: true
      }
  }
}