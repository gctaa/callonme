/*
 * Copyright (C) 2022  Jeffrey Elkner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * callonme is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import QtQuick.LocalStorage 2.7
import Ubuntu.Components 1.3

Page {
  anchors.fill: parent

  header: PageHeader {
    id: header
    title: i18n.tr('Call on Me')
    subtitle: i18n.tr('Call on and pair students fairly')

    ActionBar {
      anchors {
        top: parent.top
        right: parent.right
        topMargin: units.gu(1)
        rightMargin: units.gu(1)
      }
      numberOfSlots: 0
      actions: [
        Action {
          text: i18n.tr('Edit Class List')
        },
        Action {
          text: i18n.tr('Add/Remove Class')
        },
        Action {
          iconName: "info"
          text: i18n.tr("About")
        }
      ]
    }
  }

  Row {
    id: topRow
    spacing: units.gu(2)
    anchors {
      top: header.bottom
      left: parent.left
      right: parent.right
      topMargin: units.gu(1)
      leftMargin: units.gu(1)
      rightMargin: units.gu(1)
    }

    Label {
      id: currentClassLabel
      text: i18n.tr("Current class: ") + db_connector.currentClass
      width: 2 * (parent.width / 3) - units.gu(1)
    }
    Button {
      id: changeClassButton 
      text: i18n.tr('Change Class')
      width: parent.width / 3 - units.gu(1)
      onClicked: pages.push(Qt.resolvedUrl("ChangeClass.qml"))
    }
  }

  Label {
    id: outputDisplay
    anchors {
      top: topRow.bottom
      left: parent.left
      right: parent.right
      topMargin:  parent.height / 2 - units.gu(10)
      leftMargin: parent.width / 2 - units.gu(3)
    }
    text: db_connector.listStudents() 
  }

  Row {
    spacing: units.gu(2)
    anchors {
      bottom: parent.bottom
      left: parent.left
      right: parent.right
      topMargin: units.gu(1)
      bottomMargin: units.gu(1)
      leftMargin: units.gu(1)
      rightMargin: units.gu(1)
    }
    Button {
      id: buttonPickStudent
      text: i18n.tr('Pick Student')
      width: parent.width / 2 - units.gu(1)
    }
    Button {
      id: buttonPairStudents
      text: i18n.tr('Pair Students')
      width: parent.width / 2 - units.gu(1)
    }
  }
}
