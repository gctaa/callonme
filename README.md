# Call on Me

An Ubuntu Touch app to randomly select and pair students from a class list.

## License

Copyright (C) 2022  Jeffrey Elkner

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License version 3, as published by the Free
Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY
QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.

# Icon

Special thanks to [Freepik](<a
href="https://www.flaticon.com/free-icons/student" title="student
icons">Student icons created by Freepik - Flaticon</a>) for providing the icon
used in this app.
